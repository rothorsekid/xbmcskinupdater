import os, sys
import Tkinter, Tkconstants
import tkFileDialog
import xml.etree.ElementTree as ET
from sets import Set

Tkinter.Tk().withdraw() # we don't want a full GUI, so keep the root window from appearing

class KodiSkinUpdater:
	CHANGES_HELIX = (
		('AddonBrowser', 				'5', 'type', 'button'),										# http://forum.xbmc.org/showthread.php?tid=194515&pid=1755459#pid1755459
		('DialogAddonInfo',				'12', 'new', {"type": "button", "id": "12"}),				# http://forum.xbmc.org/showthread.php?tid=194515&pid=1712602#pid1712602
		('DialogContentSettings', 		'3', 'id', '20'),											# http://forum.xbmc.org/showthread.php?tid=194515&pid=1703508#pid1703508
		('DialogContentSettings', 		'4', 'id', '21'),											# -"-
		('DialogContentSettings', 		'6', 'id', '22'),											# -"-
		('DialogKeyboard', 				'310', 'type', 'edit'),										# http://forum.xbmc.org/showthread.php?tid=194515&pid=1769503#pid1769503
		('DialogKeyboard', 				'310', 'id', '312'),										# -"-
		('DialogMediaFilter', 			'10', 'id', '13'),											# http://forum.xbmc.org/showthread.php?tid=194515&pid=1703508#pid1703508
		('DialogMediaSource', 			'12', 'type', 'edit'),										# http://forum.xbmc.org/showthread.php?tid=194515&pid=1718557#pid1718557
		('DialogNetworkSetup', 			'11', 'type', 'edit'),										# -"-
		('DialogNetworkSetup', 			'13', 'type', 'edit'),										# -"-
		('DialogNetworkSetup', 			'14', 'type', 'edit'),										# -"-
		('DialogNetworkSetup', 			'15', 'type', 'edit'),										# -"-
		('DialogNetworkSetup', 			'16', 'type', 'edit'),										# -"-
		('DialogPeripheralSettings', 	'3', 'remove', None),										# http://forum.xbmc.org/showthread.php?tid=194515&pid=1703508#pid1703508
		('DialogPeripheralSettings', 	'10', 'id', '13'),											# -"-
		('DialogPVRTimerSettings', 		'10', 'id', '13'),											# -"-
		('SmartPlaylistEditor',			'12', 'type', 'edit'),										# http://forum.xbmc.org/showthread.php?tid=194515&pid=1718557#pid1718557
		('SmartPlaylistRule', 			'17', 'type', 'edit'),										# -"-
		('LockSettings', 				'2', 'new', {"type": "label", "id": "2"}),					# http://forum.xbmc.org/showthread.php?tid=194515&pid=1703508#pid1703508
		('ProfileSettings', 			'2', 'id', '101'),											# -"-
		('ProfileSettings', 			'2', 'new', {"type": "label", "id": "2"}),					# -"-
		('ProfileSettings', 			'1000', 'id', '102'),										# -"-
		('ProfileSettings', 			'1001', 'id', '103'),										# -"-
		('SettingsCategory', 			'13', 'new', {"type": "sliderex", "id": "13"}),				# -"- 
		('VideoOSDSettings', 			'3', 'remove', None),										# -"-
		('VideoOSDSettings', 			'10', 'id', '13'),											# -"-
		)

	def __init__(self):
		options = {}
		options["title"] = "Choose your Skin Directory"
		options["initialdir"] = os.getenv('APPDATA') + "/Kodi/addons/skin.epic"
		self.skin_dir = tkFileDialog.askdirectory(**options)
		print "Chosen Skin Directory: ", self.skin_dir
		skin_files = os.listdir(self.skin_dir)
		if 'addon.xml' in skin_files:
			print "addon.xml found, reading files..."
			self.addon_xml = ET.parse('\\'.join([self.skin_dir, 'addon.xml']))
			self.resolution_dirs = Set(skin_files) & Set(['480p', '720p', '1080p', '1080i'])
			if len(self.resolution_dirs) == 0:
				print "Resolution-specific directory was not found!"
				sys.exit(1)
		else:
			print "addon.xml was not found!"
			sys.exit(1)

	def find_control(self, control_id):
		control = self.parsed_xml.getroot().find(".//control[@id='" + control_id + "']")
		return control

	def update_control(self, control_id, attrib, new_value):
		print '\t\tUpdating control id="' + control_id + '": Changing', attrib, 'to', new_value
		control = self.find_control(control_id)
		if control is not None:
			control.set(attrib, new_value)
		else:
			print '\t\t\tControl not found!'
		
	def gotham2helix(self):
		for dir in self.resolution_dirs:
			res_dir = self.skin_dir + "/" + dir
			print "Processing resolution-specific directory '" + res_dir + "'..."
			res_dir_files = os.listdir(res_dir)
			
			# Label changes: http://forum.xbmc.org/showthread.php?tid=194515&pid=1718582#pid1718582
			print '\nImplementing label positioning changes...'
			for xmlfilename in res_dir_files:
				self.filepath = '/'.join([res_dir, xmlfilename])
				print '\t', self.filepath
				try:
					self.parsed_xml = ET.parse(self.filepath)
				except ET.ParseError:
					print "\t\tParse Error in file. Skipping."
					continue
				except IOError:
					print "\t\tSkipping Directory."
					continue

				root = self.parsed_xml.getroot()

				lists = root.findall(".//control[@type='list']") + \
					root.findall(".//control[@type='grouplist']") + \
					root.findall(".//control[@type='fixedlist']") + \
					root.findall(".//control[@type='wraplist']")
				if not lists:
					print "\t\tNo containers found."
					continue
				changed = False
				for list_container in lists:
					print "\t\tContainer:", list_container.get('type')
					labels = list_container.findall(".//control[@type='label']")
					for label in labels:
						align = label.find('align')
						if align is not None and (align.text == 'center' or align.text == 'right'):
							left = label.find('left')
							if left is not None:
								try:
									left_val = int(left.text)
								except TypeError:
									print "\t\t\tFound <left>XXr</left> Tag. Doing Nothing."
								else:
									try:
										width_val = int(label.find('width').text)
									except ValueError:
										print "\t\t\tFound <width>auto</width> Tag. Doing Nothing."
									else:
										print "\t\t\tLabel alignment", align.text, "Left:", left_val, "Width:", width_val
										if align.text == 'center':
											width_val /= 2
										newleft = left_val - width_val
										print "\t\t\t\tSetting new left value:", newleft
										left.text = str(newleft)
										changed = True
				if changed:
					print '\t\tWriting file', self.filepath
					self.parsed_xml.write(self.filepath, 'UTF-8', True)
						
			print '\nImplementing control changes...'
			self.filepath = ""
			for xmlfile, changeid, changetype, changeval in self.CHANGES_HELIX:
				filepath = '/'.join([res_dir, xmlfile + '.xml'])
				if self.filepath != filepath:
					if self.filepath != "":
						print '\t\tWriting file', self.filepath
						self.parsed_xml.write(self.filepath, 'UTF-8', True)
					self.filepath = filepath
					print '\t', filepath
					try:
						self.parsed_xml = ET.parse(filepath)
					except ET.ParseError:
						print "\t\tParse Error in file. Skipping."
						continue
					root = self.parsed_xml.getroot()
				if changetype == "new":
					if self.find_control(changeid) is None:
						print '\t\tAdding new control id="' + changeid + '"...'
						ET.SubElement(root.find("./controls"), 'control', changeval).append(ET.Comment('Element added by xbmcskinupdater'))
				elif changetype == "remove":
					control = self.find_control(changeid)
					parent = root.find(".//control[@id='" + changeid + "']..")
					if control is not None:
						print '\t\tRemoving control id="' + changeid + '"...'
						parent.remove(control)
				else:
					try:
						self.update_control(changeid, changetype, changeval)
					except IOError:
						print '\t\tFile not found!'

			print
			xbmcgui = self.addon_xml.getroot().find("./requires/import[@addon='xbmc.gui']")
			xbmcguiversion = xbmcgui.attrib['version']
			if xbmcguiversion == '5.3.0':
				print "  Version seems to be correct."
			else:
				print "Addon version outdated, bumping..."
				xbmcgui.set('version', '5.3.0')
			self.addon_xml.write(self.skin_dir + '/addon.xml', 'UTF-8', True)

if __name__=='__main__':
	updater = KodiSkinUpdater()
	updater.gotham2helix()
